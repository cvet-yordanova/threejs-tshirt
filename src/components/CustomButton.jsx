import React, {useState, useEffect} from 'react';
import { useSnapshot } from 'valtio';
import { AnimatePresence, motion } from 'framer-motion';

import {getContrastingColor} from '../config/helpers'

import state from '../store'

const Customizer = ({type, title, customStyles, handleClick}) => {
    const snap = useSnapshot(state);

    const generateStyle = (type) => {
        if(type === 'filled') {
            return {
                backgroundColor: snap.color,
                color: getContrastingColor(snap.color)
            }
        }else if(type === 'outline') {
            return {
                borderWidth: '1px',
                borderC: snap.color,
                color: snap.color
            }
        }
    }
    
    return (
        <button
        className={`px-2 py-1.5 rounded-md ${customStyles}`}
        style={generateStyle(type)}
        onClick={handleClick}
        >{title}</button>
    )
}

export default Customizer