import React, { useRef } from 'react';

import { AccumulativeShadows, RandomizedLight } from '@react-three/drei';

const Backdrop = () => {

    const shadows = useRef();
    return (
        <AccumulativeShadows red={shadows} frames={60} alphaTest={0.85} scale={10} rotation={[Math.PI / 2, 0, 0]} temporal position={[0, 0, -0.14]}>
            <RandomizedLight amount={4} 
            radius={9} 
            intensity={1} 
            ambient={0.25} position={[5, 5, -10]}/>

        <RandomizedLight amount={4} 
            radius={5} 
            intensity={0.8} 
            ambient={0.25} 
            position={[5, 5, -10]}/>
        </AccumulativeShadows>
    )
}

export default Backdrop;